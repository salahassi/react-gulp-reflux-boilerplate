var React = require('react');
var Reflux = require('reflux');
var Link = require('react-router').Link;
var PostsActions = require('../../actions/posts_actions');
var PostsStore = require('../../stores/posts_store');

var Posts = React.createClass({
    mixins: [Reflux.connect(PostsStore), Reflux.listenTo(PostsStore, ['onDataRecieved'])],
    onDataRecieved: function (d) {
        //console.log(this.state.content);
        this.setState({
            content: (
                <ul>
                    {this.state.posts.map(function (item, i) {
                        console.log();
                        return (
                            <li key={i}>
                                <div>
                                    <b>Title: </b>
                                    <a href={'/post/' + item.id}>{item.title}</a>
                                    
                                </div>
                                <div>
                                    <b>Body: </b> {item.body}
                                </div>
                                <div>
                                    <b>id: </b> {item.id}
                                </div>
                            </li>
                        );
                    })}
                </ul>
            )
        });
        console.log('data recieved');
    },
    componentWillMount: function() {
        PostsActions.requestPosts();
        this.setState({
            content: "Loading..."
        });
    },
    componentDidMount: function () {
        console.log(this.state.posts);
    },
    render: function () {
        return (
            <div>
                {this.state.content}
            </div>
        );
    }
});

module.exports = Posts;
