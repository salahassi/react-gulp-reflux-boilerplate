var React = require('react');
var Reflux = require('reflux');
var PostsActions = require('../../actions/posts_actions');
var PostsStore = require('../../stores/posts_store');

var Post = React.createClass({
    mixins: [Reflux.connect(PostsStore)],
    componentWillMount: function() {
        console.log(this.props.params.id);
        PostsActions.requestPost(this.props.params.id);
    },
    render: function () {
        return (
            <div>
                <div>
                    <b>Title: </b> {this.state.post.title}
                </div>
                <div>
                    <b>Body: </b> {this.state.post.body}
                </div>
                <div>
                    <b>id: </b> {this.state.post.id}
                </div>
            </div>
        );
    }
});

module.exports = Post;
