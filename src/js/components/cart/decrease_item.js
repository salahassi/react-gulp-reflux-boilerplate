var React = require('react');
var AppActions = require('../../actions/app_actions');

var DecreaseItem = React.createClass({
    clickHandler: function () {

        //this.props.index is passed from catalog component
        AppActions.decreaseItem(this.props.index);
    },
    render: function () {
        return (
            <button onClick={this.clickHandler} className={'btn btn-red'}>-</button>
        );
    }
});
module.exports = DecreaseItem;
