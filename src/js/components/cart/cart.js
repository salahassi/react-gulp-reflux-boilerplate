var React = require('react');
var Reflux = require('reflux');
var HeaderStatics = require('../../statics/header_statics');
var AppStore = require('../../stores/app_store');
var RemoveFromCart = require('./remove_from_cart');
var IncreaseItem = require('./increase_item');
var DecreaseItem = require('./decrease_item');


var Cart = React.createClass({
    mixins: [Reflux.connect(AppStore)],
    render: function () {
        // HeaderStatics.updateTitle("this is Cart");
        // HeaderStatics.og.title = "this is my cart";

        var total = 0;
        var items = this.state.cart.map(function (item , i) {
            var subtotal = item.cost * item.qty;
            total += subtotal;
            return (
                <tr key={i}>
                    <td>{item.title}</td>
                    <td>${item.cost}</td>
                    <td>{item.qty}<IncreaseItem index={i}/><DecreaseItem index={i}/></td>
                    <td><RemoveFromCart index={i} /></td>
                </tr>
            );
        });
        return (
            <HeaderStatics title="this is cart component" og={{title: "this is og title"}}>
                <table>
                    <thead>
                        <tr>
                            <th data-field="title">Title</th>
                            <th data-field="cost">Cost</th>
                            <th data-field="qty">QTY</th>
                            <th data-field="remove"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {items}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>TOTAL:</td>
                            <td>${total}</td>
                        </tr>
                    </tfoot>
                </table>
            </HeaderStatics>
        );
    }
});

module.exports = Cart;
