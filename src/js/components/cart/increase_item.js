var React = require('react');
var AppActions = require('../../actions/app_actions');

var IncreaseItem = React.createClass({
    clickHandler: function () {
        AppActions.increaseItem(this.props.index);
    },
    render: function () {
        return (
            <button onClick={this.clickHandler} className={'btn btn-red'}>+</button>
        );
    }
});
module.exports = IncreaseItem;
