var React = require('react');
var AppActions = require('../../actions/app_actions');

var RemoveFromCart = React.createClass({
    clickHandler: function () {
        AppActions.removeItem(this.props.index);
    },
    render: function () {
        return (
            <button onClick={this.clickHandler} className={'btn btn-red'}>x</button>
        );
    }
});
module.exports = RemoveFromCart;
