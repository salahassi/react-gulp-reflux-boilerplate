var React = require('react');
var Link = require('react-router').Link;
var translate = require('../../mixins/translate');
var CartSummary = require('./cart_summary');

var Header = React.createClass({
    mixins: [translate],
    render: function () {
        return (
            <nav>
              <div className={"nav-wrapper container"}>
                <Link to="/" query={this.query()} className={"brand-logo"}>
                    Logo
                </Link>
                <CartSummary />
              </div>
            </nav>
        );
    }
});

module.exports = Header;
