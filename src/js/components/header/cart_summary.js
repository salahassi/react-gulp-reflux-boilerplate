var React = require('react');
var Reflux = require('reflux');
var Link = require('react-router').Link;
var AppStore = require('../../stores/app_store');
var translate = require('../../mixins/translate');

var CartSummary = React.createClass({
    mixins: [translate, Reflux.connect(AppStore)],

    render: function () {
        return (
            <ul className={"right hide-on-med-and-down"}>
                <li>
                    <Link to="cart" query={this.query()}>
                      Cart Items: {this.state.cart.length} / ${this.state.cartTotal}
                    </Link>
                </li>
            </ul>
        );
    }
});

module.exports = CartSummary;
