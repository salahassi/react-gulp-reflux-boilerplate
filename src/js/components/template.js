var React = require('react');
var Header = require('./header/header');
var router = require('react-router');
var Template = React.createClass({
    render: function () {
        return (
            <div>
                <Header />
                <div className={'container'}>
                    {this.props.children}
                </div>
            </div>
        );
    }
});

module.exports = Template;
