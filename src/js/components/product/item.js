var React = require('react');
var Link = require('react-router').Link;
var translate = require('../../mixins/translate');
var AddToCart = require('../catalog/add_to_cart');

var Item = React.createClass({
    mixins: [translate],
    render: function () {

        return (
            <div className="card">
              <div className="card-image">
                <Link to="item" query={this.query()} params={{item: this.props.item.id}}>
                    <img src={this.props.item.img} />
                </Link>
                <span className="card-title">{this.props.item.title}</span>
              </div>
              <div className="card-content">
                <p>{this.props.item.summary}</p>
                <p>{this._('validation.username')}</p>
              </div>
              <div className="card-action">
                    <div className="row">
                        <div className="col s6">
                            ${this.props.item.cost}
                        </div>
                        <div className="col s6">
                            <AddToCart item={this.props.item}/>
                        </div>

                    </div>
              </div>
            </div>
        );
    }
});

module.exports = Item;
