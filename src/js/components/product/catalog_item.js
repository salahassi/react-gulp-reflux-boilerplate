var React = require('react');
var Item = require('./item');

var CatalogItem = React.createClass({
    render: function () {
        return (
            // <div className="row">
              <div className="col s12 m4">
                <Item item={this.props.item} />
              </div>
            // </div>
        );
    }
});

module.exports = CatalogItem;
