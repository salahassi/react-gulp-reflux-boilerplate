var React = require('react');
var Reflux = require('reflux');
var AppStore = require('../../stores/app_store');
var Item = require('./item');

var CatalogItem = React.createClass({
    render: function () {
        //console.log(this.props);
        return (
            // <div className="row">
              <div className="col s12 m12">
                <Item item={AppStore.getItem(this.props.params.item)} />
              </div>
            // </div>
        );
    }
});

module.exports = CatalogItem;
