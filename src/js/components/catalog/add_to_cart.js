var React = require('react');
var AppActions = require('../../actions/app_actions');

var AddToCart = React.createClass({
    clickHandler: function () {
        AppActions.addItem(this.props.item);
    },
    render: function () {
        return (
            <button onClick={this.clickHandler} className={'btn btn-red'}>Add To Cart</button>
        );
    }
});
module.exports = AddToCart;
