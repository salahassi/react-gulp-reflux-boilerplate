var React = require('react');
var Reflux = require('reflux');
var HeaderStatics = require('../../statics/header_statics');
var AppStore = require('../../stores/app_store');
var CatalogItem = require('../product/catalog_item');

var Catalog = React.createClass({
    mixins: [Reflux.connect(AppStore)],
    render: function () {
        //HeaderStatics.updateTitle("this is Catalog");
        return (
            <HeaderStatics title="this is catalog component" >
                <div className="row">
                    {this.state.catalog.map(function (item , i) {
                        return (

                            <CatalogItem key={i} item={item}  />
                        );
                    })}
                </div>
            </HeaderStatics>
        );
    }
});

module.exports = Catalog;
