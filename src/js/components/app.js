var React = require('react');
var Router = require('react-router');

var RouteHandler = Router.RouteHandler;

var Template = require('./template.js');

var App = React.createClass({
    render: function () {

        return (
            <Template>
                <RouteHandler />
            </Template>
        );
    }
});
module.exports = App;
