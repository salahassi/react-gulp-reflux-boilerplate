var React = require('react');

HeaderStatics = React.createClass({
    statics: {
        title: "this is page title",
        meta: {
            description: "this is description",
            keywords: "this, is, keywords, meta"
        },
        og: {
            title: "this is title"
        }
    },
    componentWillMount: function () {
        //console.log(this.props.title);
        if(this.props.title)
            HeaderStatics.title = this.props.title;

        if(this.props.meta)
            HeaderStatics.meta = this.props.meta;

        if(this.props.og)
            HeaderStatics.og = this.props.og;
    },
    componentDidMount: function () {
        //console.log(this.props.title);
        if(this.props.title) {
            if(typeof document != 'undefined')
                document.title = HeaderStatics.title;
        }
    },

    render: function () {
        //console.log(this.props);
        return this.props.children;
        //console.log('rendered');
    }
});

module.exports = HeaderStatics;
