var React = require('react');
var Router = require('react-router');

var routes = require('./routes');

window.onload = function () {
    Router.run(routes, Router.HistoryLocation, function (Handler) {
        NProgress.start();
        React.render(<Handler />, document.getElementById('main'), function () {
            NProgress.done();
        });

    });
}
