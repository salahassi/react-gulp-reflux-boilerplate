var Reflux = require('reflux');

var PostsActions = Reflux.createActions(
    ['requestPosts', 'requestPost', 'dataRecieved']
);

module.exports = PostsActions;
