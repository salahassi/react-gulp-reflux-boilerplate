var Reflux = require('reflux');
//here we go with actions
//we just declare them and add them to the queue (dispatcher)
var AppActions = Reflux.createActions(
    ['addItem', 'removeItem', 'increaseItem', 'decreaseItem']
);

module.exports = AppActions;
