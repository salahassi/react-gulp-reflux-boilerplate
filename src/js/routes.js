var React = require('react');
var Router = require('react-router'); // or var Router = ReactRouter; in browsers

var DefaultRoute = Router.DefaultRoute;
var Route = Router.Route;

var App = require('./components/app');
var Catalog = require('./components/catalog/catalog');
var Cart = require('./components/cart/cart');
var Posts = require('./components/posts/posts');
var Post = require('./components/posts/post');
var CatalogItem = require('./components/product/show_item');

var routes = (
    <Route handler={App} path="/">
        <DefaultRoute name="home" handler={Catalog} />
        <Route name="cart" path="/cart" handler={Cart}/>
        <Route name="posts" path="posts" handler={Posts}/>
        <Route name="post" path="post/:id" handler={Post}/>

        <Route name="item" path="/item/:item" handler={CatalogItem}/>
    </Route>
);

module.exports = routes;
