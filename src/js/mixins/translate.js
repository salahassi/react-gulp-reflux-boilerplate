var React = require('react');
var State = require('react-router').State;

var default_lang = 'en';

function getValueByArray(obj, parts, value){

    if(!parts) {
        return null;
    }

    if(parts.length === 1){
        return obj[parts[0]];
    } else {
        var next = parts.shift();

        if(!obj[next]){
          return null;
        }
        return getValueByArray(obj[next], parts, value);
    }
}

module.exports = {
    mixins: [State],
    _: function(key) {
        var lang = this.getQuery().lang ? this.getQuery().lang : default_lang;
        var localization = require('../langs/all')[lang];
        splits = key.split('.');
        return getValueByArray(localization, splits);
    },
    query: function(obj) {
        var query = this.getQuery().lang ? {lang: this.getQuery().lang} : {};
        for(var i in obj) {
            query[i] = obj[i];
        }
        return query;
    }
}
