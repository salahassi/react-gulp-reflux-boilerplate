var Reflux = require('reflux');
var AppActions = require('../actions/app_actions');

//APIs, Logic and data calls are handled here
var _catalog = [];

for(var i = 0; i < 10; i++) {
    var item = {
        id: "widget" + i,
        title: "Widget #" + i,
        summary: "this is the summary of the item",
        description: "this is a long description of the item that describes all details about it",
        img: '/assets/img/product_img.png',
        cost: i
    };
    _catalog.push(item);
}

var _cartItems = [];

//private functions which handle functionality
function _removeItem(index) {
    _cartItems[index].inCart = false;
    _cartItems.splice(index, 1);
}

function _increaseItem(index) {
    _cartItems[index].qty++;
}

function _decreaseItem(index) {
    if(_cartItems[index].qty > 1) {
        _cartItems[index].qty--;
    } else
        _removeItem(index);
}

function _addItem(item) {
    // console.log("_addItem");
    if(!item.inCart) {
        item.qty = 1;
        item.inCart = true;
        _cartItems.push(item);
    } else {
        _cartItems.forEach(function(cartItem, i) {
            if(cartItem.id === item.id) {
                _increaseItem(i);
            }
        });
    }
}
function getTotalCost () {
    var sum = 0;
    for(var i in _cartItems) {
        sum += _cartItems[i].qty * _cartItems[i].cost;
    }
    //console.log(summary);
    return sum;
}


var AppStore = Reflux.createStore({
    listenables: [AppActions],
    tr: function () {
        this.trigger({
            catalog: _catalog,
            cart: _cartItems,
            cartTotal: getTotalCost()
        });
    },
    getInitialState: function () {
        return {
            catalog: _catalog,
            cart: _cartItems,
            cartTotal: 0
        }
    },
    onAddItem: function (item) {
        _addItem(item);
        this.tr();
    },
    onRemoveItem: function (index) {
        _removeItem(index);
        this.tr();
    },
    onIncreaseItem: function (index) {
        _increaseItem(index);
        this.tr();
    },
    onDecreaseItem: function (index) {
        _decreaseItem(index);
        this.tr();
    },
    getCart: function () {
        return _cartItems;
    },
    getCatalog: function () {
        return _catalog;
    },
    getItem: function (id) {
        //console.log('getItem ' + id);
        for (var i in _catalog) {
            if(_catalog[i].id === id) {
                //console.log('found');
                return _catalog[i];
            }
        }

        return {};
    }
});

module.exports = AppStore;
