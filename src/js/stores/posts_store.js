var React = require('react');
var Reflux = require('reflux');
var PostsActions = require('../actions/posts_actions');
var request = require('superagent');

var PostsStore = Reflux.createStore({
    listenables: [PostsActions],
    data: {
        posts: [],
        post: {},
        //content: null
    },
    getInitialState: function () {
        return this.data;
    },
    onRequestPosts: function () {
        var that = this;
        request.get('http://jsonplaceholder.typicode.com/posts')
        .end(function(err, r){
            that.data.posts = r.body;
            that.data.post = {};
            that.trigger(that.data);
            PostsActions.dataRecieved(r);
        });
    },
    onRequestPost: function(id) {
        var that = this;
        console.log('http://jsonplaceholder.typicode.com/posts/' + id);
        request('http://jsonplaceholder.typicode.com/posts/' + id)
        .end(function(err, r){
            that.data.post = r.body;
            that.data.posts = [];
            that.trigger(that.data);
            PostsActions.dataRecieved(r);
        });;
    }
});

module.exports = PostsStore;
