require('node-jsx').install({extension: '.js'});
var express = require('express');
var React = require('react');
var Router = require('react-router');
routes = require('./js/routes');
var fs = require('fs');
var path = require('path');
var app = express();

var HeaderStatics = require('./js/statics/header_statics');

var HOME = fs.readFileSync(__dirname + '/index.html', {encoding: 'utf8'});

app.get('/favicon.ico', function (req, res) {
    res.send('');
});

app.use('/assets', express.static(__dirname + '/assets'));

app.use(function (req, res, next) {
    console.log(req.url);
    var router = Router.create({location: req.url ? req.url : "/", routes: routes});
    router.run(function(Handler) {

            var factory = React.createFactory(Handler)();
            var html = React.renderToString(factory);

            var header = '';
            if(HeaderStatics.title)
                header += '<title>' + HeaderStatics.title + '</title>';

            if(HeaderStatics.meta) {
                for (var i in HeaderStatics.meta) {
                    header += '<meta name="'+ i +'" content="'+ HeaderStatics.meta[i] +'">';
                }
            }

            if(HeaderStatics.og) {
                for(var i in HeaderStatics.og) {
                    header += '<meta property="og:'+ i +'" content="'+ HeaderStatics.og[i] +'" />';
                }
            }


            HOME = HOME.replace(/<!-- header -->[\s\S]*?<!-- \/header -->/, '<!-- header -->' + header + '<!-- \/header -->');
            HOME = HOME.replace(/<!-- body -->[\s\S]*?<!-- \/body -->/, '<!-- body -->' + html + '<!-- \/body -->');

        res.send(HOME);
    });
    //res.end(HOME);
});

app.listen(3000);
