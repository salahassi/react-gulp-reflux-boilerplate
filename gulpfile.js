var gulp = require('gulp');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var server = require('gulp-connect');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

//browserify command concats all js files in src/js
//and converts them into js (from jsx)
//and then add them in main.js in dist/js
gulp.task('browserify', function () {
    gulp.src('src/js/main.js')
        .pipe(browserify({transform: 'reactify'}))
        .pipe(uglify())
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('src/assets/js'))
        .pipe(server.reload());
});

gulp.task('sass', function () {
  gulp.src('src/sass/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('src/assets/css'));
});


//copy command to coppy index.html from src to dist folder
// gulp.task('copy', function () {
//     gulp.src('src/index.html')
//         .pipe(gulp.dest('dist'));
//
//     gulp.src('src/assets/**/*.*')
//         .pipe(gulp.dest('dist/assets'));
//
//     gulp.src('src/assets/css/*.*')
//         .pipe(gulp.dest('dist/assets/css'));
//
// });

//connect command starts the server
gulp.task('connect', function() {
    server.server({
        port: 3000,
        root: 'src',
        livereload: true
    });
});

//default command that runs browserify and copy tasks
gulp.task('default', ['browserify', /*'copy',*/ 'sass']);

//watchs all files in src, if anything changes, default task will run
gulp.task('watch', function() {
    gulp.watch(['src/js/**/*.*', 'src/index.html', 'src/server.js'], ['default', 'browserify']);
});

gulp.task('serve', ['watch', 'connect']);
